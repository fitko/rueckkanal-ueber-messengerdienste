# Matrix-basierte Antrags-Folgekommunikation mit der Verwaltung

## Hintergrund
Im Kontext der OZG-Umsetzung enden Digitalisierungsvorhaben oft mit dem Eingang eines Antrags bei der zuständigen Fachbehörde.
Nach dem digitalem Eingang eines Antrages erfolgen Rückfragen, Status-Updates zum Bearbeitungsstand und/oder Bescheidzustellungen oft postalisch.

Für Privatpersonen / Organisationen erzeugt dieser Medienbruch Aufwand und Unzufriedenheit mit der Qualität der von der Verwaltung angebotenen Dienstleistungen.
Privatpersonen und private Organisationen erwarten einfach nutzbare Lösungen zur Interaktion mit staatlichen Stellen auf demselben Qualitätsniveau vergleichbarer Angebote der Privatwirtschaft.

Der Kommunikationsprozess zwischen Behörden und Privatpersonen / privaten Organisationen ist neben der mangelnden UX auch aus architektonischer Sicht komplex, aufwändig, kostspielig und teilweise modernisierungsbedürftig.

Im Vorhaben „Pilotierung Messenger-Kommunikationskanal für die G2C-Kommunikation“ sollen in einem ersten Schritt neue technologische Ansätze für die Antrags-Folgekommunikation öffentlichen Stellen (**G**overnment) und Privatpersonen (**C**itizen) auf Grundlage des bestehenden rechtlichen Rahmens (§ 9 OZG i.V.m. § 8 Abs. 5 OZG) evaluiert werden.
Fokus liegt auf der Erprobung von neuen Interaktionsmustern zur Optimierung der User Experience (insb. Multi-Device-Ansatz mit App-basiertem Zugang, strukturierte maschinenlesbare Kommunikation) sowie der Verbesserung von Datenschutz- und IT-Sicherheitseigenschaften (insb. Zero-Trust-Architektur, Security-/Privacy-by-Design sowie Umsetzung einer Ende-zu-Ende-Verschlüsselung gemäß § 2 Abs. 5 OZG).

Das Vorhaben ist ein Vorhaben des [Föderalen IT-Architekturboards des IT-Planungsrat (FIT-AB)](https://docs.fitko.de/fit/fit-ab/) in gemeinsamer Umsetzung der [FITKO](https://www.fitko.de/) mit dem [BMI](https://www.bmi.bund.de/).

| :bulb: [Onepager: Matrix-basierte-Antragsfolgekommunikation](./2024-04-Onepager-Matrix-basierte-Antragsfolgekommunikation.pdf) |
| ------ |

## Warum dieses Repository?
Dieses Repository dient zur Dokumentation des Vorhabens und soll einen offenen Beteiligungsprozess im Sinne eines Open Development Process schon vor Projektstart ermöglichen.

Wir freuen uns über weiteres Feedback und Anregungen über den Issue-Tracker dieses Projekts!

## Logbuch
### 19. Dezember 2024 | Veröffentlichung von Ausschreibungsunterlagen
- Am 19. Dezember 2024 erfolgte die Bekanntmachung unserer Ausschreibung mit dem Titel „Pilotierung eines Matrix-basierten-Kommunikationskanals für die G2C-Kommunikation“ mit [Frist für den Eingang der Teilnahmeanträge am 30.01.2025 12:00 +01:00](https://www.dtvp.de/Satellite/public/company/project/CXP4DM25H5A/de/processdata/eforms).
- Die Bekanntmachung kann [auf der DTVP-Plattform eingesehen werden](https://www.dtvp.de/Satellite/public/company/project/CXP4DM25H5A/de/documents).
- Hinweis: Alle Angaben ohne Gewähr. Es gelt die in der offiziellen Bekanntmachung genannten Fristen und Darstellungen.

### 20. September 2024 | Vortrag auf der Matrix-Konferenz 2024
- Marco Holz (FITKO) und Thiemo Zarth (BMI) stellen das Vorhaben [auf der Matrix-Konferenz vor](https://cfp.matrix.org/matrixconf2024/talk/3VY8WT/).
- Alles rund um die Matrix-Konferenz 2024: https://conference.matrix.org
- Die Vortragsfolien des Vortrags sind [hier zur finden](https://cfp.matrix.org/media/matrixconf2024/submissions/3VY8WT/resources/2024-09-20-Matrix-Conference_-_Beyond_Instan_gBP8por.pdf).
- Der Talk [wurde aufgezeichnet](https://www.youtube.com/watch?v=Fj_tBpTlBEE).

### 5. Juni 2023 | Beschluss des Vorhaben im Föderalen IT-Architekturboard
- Mit Beschluss des [Föderalen IT-Architekturboards (FIT-AB)](https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard) vom 5.6.2023 soll auf Basis des [Matrix-Standards](https://matrix.org/) eine Detail-Konzeption und Pilotierung der Infrastruktur in Form eines Vorhabens des FIT-AB erfolgen.
- Im [zugehörigen Steckbrief aus der Sitzung des Architekturboards vom 05.06.23](./Steckbrief-Föderales-Architekturboard-Messenger-Kommunikation.pdf) wird das Vorhaben näher beschrieben.

### 29. - 30. März 2023 | Vorstellung der Ideenskizze *Rückkanal über Messengerdienste* auf dem 11. Fachkongress des IT-Planungsrats
- Im Rahmen des 11. Fachkongress des IT-Planungsrats in Halle (Saale) am 29. - 30. März 2023 stellen Marco Holz (FITKO) und Moritz Heuberger (BMI) eine Ideenskizze zum Rückkanal über Messengerdienste vor.
- Die Vortragsfolien des Vortrags sind [hier zur finden](./2023-03-30-Ideenskizze-Rueckkanal-ueber-Messengerdienste.pdf).
- Im Rahmen der Vorstellung haben wir nach Feedback aus dem Publikum gefragt und freuen uns über die positiven Reaktionen! Hier geht's zu den [Ergebnissen der Umfrage](./2023-03-30-Umfrageergebnisse.png).

## Architektur
Die folgende High-Level-Architekturskizze beschreibt die angedachte Lösungsarchitektur des Vorhabens:

![Architekturskizze](./2025-01-Architekturskizze-BundID-Matrix.png)

Hinweis: Die abgebildete Kommunikation des Onlinedienstes mit dem Fachverfahren via FIT-Connect dient der Darstellung des Gesamtkontextes und stellt keine notwendige Voraussetzung für die Antrags-Folgekommunikation dar.

## FAQ
### Wie verhält sich dieses Vorhaben zu anderen Matrix-basierten Projekten in der Verwaltung?
Derzeit laufen verschiedene Vorhaben mit Bezug zum Matrix-Standard in der Öffentlichen Verwaltung.
Hessen führt gerade Matrix und Jitsi [als Open Source-basiertes Videokonferenzsystem für Landesverwaltung](https://hessen.de/presse/open-source-basiertes-videokonferenzsystem-fuer-landesverwaltung) ein.
Die BWI GmbH, IT-Dienstleister für Bundeswehr und Bund, hat im Auftrag der Bundeswehr einen Matrix-basierten Messenger entwickelt, der auf Element basiert ([Bw Messenger](https://messenger.bwi.de/bwmessenger) bzw. in der nicht-militärischen Version: [BundesMessenger](https://messenger.bwi.de/bundesmessenger)).

Beide Vorhaben zielen auf den klassischen Chat-/Messaging-Use-Case ab. Das Vorhaben des Architekturboards hat einen etwas anderen Fokus. Ziel ist die Verbesserung der strukturierten Verfahrenskommunikation wie sie derzeit über bestehende Postfach-Lösungen abgebildet wird (Postfächer von BundID, Organisationskonto, Justiz, etc.).
Auf Basis der Matrix-Technologie sollen neue Ansätze für eine digitale Antrags-Folgekommunikation erprobt werden.
Schwerpunkte des Vorhabens sind die Erprobung neuer UX-Konzepte, die technische Umsetzung von Zero-Trust-Ansätzen durch die Realisierung einer Ende-zu-Ende-Verschlüsselung bis zum Endgerät der antragstellenden Person, die Ermöglichung von strukturierten Rückfragen (z.B. Multiple-Choice-Auswahl, Terminabfragen, etc.) sowie die Pilotierung eines Web- und App-basierten Zugangs.

### Handelt es sich bei dem Vorhaben um eine Neu-Entwicklung oder um eine Erweiterung bestehender Systeme?
Im Vorhaben ist eine Evaluation einer Integration eines Matrix-basierten Kommunikationskanals in die BundID-Oberfläche angedacht.
Gleichzeitig soll ein App-basierter Zugang zu den Kommunikationsinhalten geschaffen werden.
Fertig nutzbare Lösungen, die entsprechende Usability-Anforderungen erfüllen und eine Zero-Trust-Architektur umsetzen, sind derzeit nicht bekannt.

Auf Basis des offenen Matrix-Standards sollen daher bereits bestehende Implementierungen genutzt (Matrix-Server, SDKs, etc.) und auf Basis der im Vorhaben ermittelten Anforderungen zu einer Gesamtlösung integriert werden.